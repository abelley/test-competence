<!doctype html>
<html lang="fr-CA" class="no-js">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="True">

  <!-- Hypertext / Theming -->
  <title>Test de compétence - Agence Unik Média</title>
  <meta name="description" content="Création site Web Québec - Agence Unik Média" />

  <meta name="theme-color" content="#a7201c">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

	<!-- Scripts / Styles -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
	<link href="assets/dist/main.css" rel="stylesheet" type="text/css" media="all" />
	<script src='assets/scripts/vendor/modernizr.custom.js'></script>

</head>
<body>

  <header class="header wrapper wrapper--full">

    <div class="header__logo">
      <img src="assets/images/uniklogo.png" alt="Agence Unik Média">
    </div>

  </header>

  <main class="main wrapper wrapper--small">

    <form class="form">

    	<h1 class="title-main">Formulaire de contact</h1>

    	<div class="form__fields">
    		<div class="form__field form__field--half">
    			<input type="text" name="name" id="name" placeholder="Saisir votre nom" />
    			<label>Nom</label>
    			<span class="form__field__status"></span>
    		</div>
    		<div class="form__field form__field--half">
    			<input type="email" name="email" id="email" placeholder="Saisir votre courriel" />
    			<label>Courriel</label>
    			<span class="form__field__status"></span>
    		</div>
    		<div class="form__field">
    			<textarea name="message" id="message" placeholder="Saisir votre message"></textarea>
    			<label>Message</label>
    			<span class="form__field__status"></span>
    		</div>
    	</div>

    	<div class="form__footer">
    		<div class="form__footer__status"></div>
		    <button type="submit" class="button form__submit">Envoyer<span class="form__submit__loader"></span></button>
		  </div>

		  <div class="form__success">
		  	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.4 14.6">
	        <path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M0.8,7.3l5.6,5.6l12-12"></path>
	      </svg>
      	<div class="form__success__message">Votre message a été envoyé!</div>
		  </div>

    </form>

  </main>

  <script src='assets/dist/main.js'></script>
</body>
</html>