<?php
require 'vendor/autoload.php';
use Mailgun\Mailgun;

$name = (isset($_POST['name'])) ? $_POST['name'] : null;
$email = (isset($_POST['email'])) ? $_POST['email'] : null;
$message = (isset($_POST['message'])) ? $_POST['message'] : null;

if( empty($name) || empty($email) || empty($message) ):

	$response = array(
		"is_valid" => false,
		"name" => $name,
		"email" => $email,
		"message" => $message,
	);

else:

	// Instantiate credentials
	$mgKey = Mailgun::create('2249b14b535607a11f2b0227fa468beb-e49cc42c-0a1b52cf');
	$mdDomain = "sandbox528d0e1894d44f1294665076a4c01e74.mailgun.org";

	// Make the call
	$mgKey->messages()->send($mdDomain, [
		'from'    => 'Alexandre Belley <belley.alexandre@gmail.com>',
	  'to'      => $email,
	  'subject' => "Nouveau message de {$name}",
	  'text'    => $message
	]);

	$mgKey->domains()->show($mdDomain)->getInboundDNSRecords();

	// Return response
	$response = array(
		"is_valid" => true,
		"name" => $name,
		"email" => $email,
		"message" => $message,
	);

endif;

echo json_encode($response);
exit;