/*-
 * Social shares
 *
 * @selector .js-share
 * @required [href]
-*/
function sendForm(e) {
  e.preventDefault(); // Get form values

  var ContactForm = e.target;
  var ContactFormData = new FormData(ContactForm);
  var Fields = ContactForm.querySelectorAll('.form__field');
  var FormStatusMsg = ContactForm.querySelector('.form__footer__status'); // Clear classes and messages

  ContactForm.classList.add('is-sending');
  ContactForm.classList.remove('has-error');
  Fields.forEach(function (Field) {
    Field.classList.remove('is-valid', 'is-invalid');
  });
  FormStatusMsg.innerText = ''; // Send AJAX request

  var XHR = new XMLHttpRequest();
  XHR.responseType = 'json';
  XHR.open('POST', "send_email.php");

  XHR.onload = function () {
    var FormResponse = this.response;
    ContactForm.classList.remove('is-sending'); // Error with Mailgun

    if (FormResponse == null) {
      ContactForm.classList.add('has-error');
      FormStatusMsg.innerText = "Une erreur s'est produite. \nAssurez-vous que le courriel est vérifié."; // Success
    } else if (FormResponse.is_valid) {
      ContactForm.classList.remove('is-sending');
      ContactForm.classList.add('is-success'); // Missing field values
    } else {
      Fields.forEach(function (Field, key) {
        var FieldInput = Field.querySelector('input, textarea');

        if (FieldInput.value === '') {
          Field.classList.remove('is-valid');
          Field.classList.add('is-invalid');
        } else {
          Field.classList.add('is-valid');
          Field.classList.remove('is-invalid');
        }
      });
      ContactForm.classList.add('has-error');
      FormStatusMsg.innerText = "Le formulaire ne semble pas correctement rempli. \nLes erreurs ont été mises en surbrillance.";
    }
  };

  XHR.onerror = function (e) {
    console.log(e);
    ContactForm.classList.add('has-error');
    ContactForm.classList.remove('is-sending');
    FormStatusMsg.innerText = "Une erreur s'est produite.";
  };

  setTimeout(function () {
    return XHR.send(ContactFormData);
  }, 350);
  return false;
}

window.addEventListener('load', function () {
  document.documentElement.classList.add('is-dom-loaded');
  var Forms = document.querySelectorAll('.form');
  Forms.forEach(function (Form) {
    Form.addEventListener('submit', sendForm, false);
  });
});

//# sourceMappingURL=main.js.map
