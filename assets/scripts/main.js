import sendForm from 'modules/send-email';

// When everything loaded
window.addEventListener('load', () => {

  document.documentElement.classList.add('is-dom-loaded');

  const Forms = document.querySelectorAll('.form');
  Forms.forEach(Form => {
    Form.addEventListener('submit', sendForm, false);
  });

});
