'use strict';

const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const browserSync = require("browser-sync").create();
const rename = require("gulp-rename");

const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");

const rollup = require('gulp-better-rollup');
const includePaths = require('rollup-plugin-includepaths');
const resolve = require('rollup-plugin-node-resolve');
const commonJS = require('rollup-plugin-commonjs');
const babel = require('rollup-plugin-babel');
const uglify = require('gulp-uglify-es').default;

const svgSprite = require("gulp-svg-sprite");

const config = {
  proxy: "https://unik.test",
  paths: {
    dist: "./assets/dist",
    script: "./assets/scripts",
    style: "./assets/styles",
    img: "./assets/images",
  }
};

// COMPILE
const compileStyles = () => {
  return gulp.src(`${config.paths.style}/*.scss`)

    .pipe(sourcemaps.init())
    .pipe(sass())
    .on("error", sass.logError)

    .pipe(postcss([autoprefixer()]))

    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.paths.dist))
    .pipe(browserSync.stream());
}

const compileScripts = () => {

  return gulp.src(`${config.paths.script}/main.js`)
    .pipe(sourcemaps.init())
    .pipe(rollup({
      plugins: [
        includePaths({
          paths: ['node_modules', `${config.paths.script}/vendor`]
        }),
        resolve(),
        commonJS(),
        babel({
          "presets": [
            [
              "@babel/preset-env", {
                modules: false,
                "targets": {
                  "browsers": ["> 0.25%, not dead"]
                }
              }
            ]
          ]
        })
      ]
    }, {
      format: 'es'
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.paths.dist))
    .pipe(browserSync.stream());
}

const compile = gulp.parallel(compileStyles, compileScripts);

// SVGs
const generateSprites = () => {
  return gulp.src(`${config.paths.img}/icons/*.svg`)
    .pipe(svgSprite({
      shape: {
        dimension: {
          maxWidth: 1000,
          maxHeight: 1000
        }
      },
      mode: {
        symbol: {
          bust: false,
          sprite: "sprite.svg",
          dest: '.'
        },
        stack: {
          bust: false,
          sprite: "icons.svg",
          dest: '.'
        }
      }
    }))
    .pipe(gulp.dest(config.paths.img));
}

// MINIFY
const minifyStyles = () => {
  return gulp.src(`${config.paths.dist}/main.css`)
    .pipe(rename(path => {
      path.basename += ".min";
    }))
    .pipe(postcss([cssnano()]))
    .pipe(gulp.dest(config.paths.dist));
}

const minifyScripts = () => {
  return gulp.src(`${config.paths.dist}/main.js`)
    .pipe(rename(path => {
      path.basename += ".min";
    }))
    .pipe(uglify({
      compress: {
        drop_console: true
      }
    }))
    .pipe(gulp.dest(config.paths.dist));
}

const minify = gulp.parallel(minifyStyles, minifyScripts);

// WATCH
const watchFiles = () => {
  gulp.watch(`${config.paths.style}/**/*.scss`, compileStyles);
  gulp.watch(`${config.paths.script}/**/*.js`, compileScripts);
}

const startServer = () => {
  browserSync.init({
    proxy: config.proxy
  });
}

const watch = gulp.parallel(watchFiles, startServer);

// EXPORT TASKS
exports.compile = compile;
exports.compileStyles = compileStyles;
exports.compileScripts = compileScripts;
exports.generateSprites = generateSprites;
exports.minify = minify;
exports.minifyStyles = minifyStyles;
exports.minifyScripts = minifyScripts;
exports.watch = watch;
exports.watchFiles = watchFiles;
exports.default = watch;
